CC=	cc
WFLAGS=	-Wno-unused-parameter
CFLAGS=	-O2 -g -pipe -Wall -Wextra -pedantic -I./vm $(WFLAGS)
LDFLAGS=
LEX=	lex
VMOBJS=
ASMOBJS=
ALLOBJS=
EXEC=
all:	$(EXEC)
clean:
	rm -rf $(ALLOBJS) $(EXEC)
