NeoTECH GPCS Architecture
=========================

* 64 Bit
* RISC?
* Load-Store Architecture
* 256 Registers
* Flat Address
* Single Address Space
* Byte-Orientated

Instructions:
	NAME:	TYPE / REG:		DESCRIPTION:

	Special:
	NOP	Z	(NO)		Nope
	HLT	Z	(NO)		Halt
	BRK	Z	(NO)		Break
	IDLE	Z	(NO)		CPU idle
	INT	U	(NO)		Trigger a software Interrupt, SID=%ARG
	EUM	JC	(NO)		Enter Usermode at $ADDR
	SYS	SYS	(YES)		System Call
	IO	IO	(YES)		IO Operation
	IRET	Z	(NO)		Return from Interrupt Service Routine

	Memory Reference:
	LD	LS	(YES)		Load
	ST	LS	(YES)		Store
	LB	U	(YES)		Load  8 Bit intermediate
	LI	IQ	(YES)		Load 16 Bit intermediate
	LH	IH	(YES)		Load 32 Bit intermediate
	LIW	IW	(YES)		Load 64 Bit intermediate

	Integer Arithmetic:
	ADD	D	(YES)		Add
	SUB	D	(YES)		Subtract
	INC	U	(YES)		Increment
	DEC	U	(YES)		Decrement
	MUL	D	(YES)		Multiply
	DIV	D	(YES)		Divide
	MOD	D	(YES)		Modulo
	ADDI	IQ	(YES)		Add intermediate
	SUBI	IQ	(YES)		Subtract intermediate
	INCI	IQ	(YES)		Increment intermediate
	DECI	IQ	(YES)		Decrement intermediate

	Bitwise Operation:
	AND	D	(YES)		And
	OR	D	(YES)		Or
	XOR	D	(YES)		XOR
	NOT	U	(YES)		Not
	RTR	U	(YES)		Rotate Right
	RTL	U	(YES)		Rotate Left
	SHR	U	(YES)		Shift Right
	SHL	U	(YES)		Shift Left
	RTBR	U	(YES)		Rotate Byte Right
	RTBL	U	(YES)		Rotate Byte Left
	SHBR	U	(YES)		Shift Byte Right
	SHBL	U	(YES)		Shift Byte Left

	Data Moving:
	SWP	D	(YES)		Swap content of register
	MOV	D	(YES)		Move %SRC to %DST
	MOVC	D	(YES)		Move %SRC to %DST and clear %SRC

	Stack:
	PUSH	U	(YES)		Push
	POP	U	(YES)		Pop

	Jump / Branch:
	J	JC	(NO)		Jump
	JE	CJC	(YES)		Jump if equal
	JNE	CJC	(YES)		Jump if not equal
	JGT	CJT	(YES)		Jump if greater than
	JGE	CJT	(YES)		Jump if greater than or equal
	JS	JC	(NO)		Jump to Subroutine
	RS	Zi	(NO)		Subroutine Return

	Call:
	C	JC	(NO)		Call
	CE	CJC	(YES)		Call if equal
	CNE	CJC	(YES)		Call if not equal
	CGT	CJC	(YES)		Call if greater than
	CGE	CJC	(YES)		Call if greater than or equal
	R	Z	(NO)		Return

Registers:
	R00~RFF	(64 Bit):	General Purpose Register
	ID	( 8 Bit):	Interrupt Indentify

General Purpose Register Alias:
	Name:	ALIAS:	DESCRIPTION:
	%00	Z	Zero
	%01	A	Accumulator
	%02	B	Register B
	%03	C	Counter
	%04	D	Data
	%05	PC	Program Counter
	%06	SP	Stack Pointer
	%07	SRA	Subroutine Return Address
	%08	IPC	Saved PC
	%09	SID	Software Interrupt Identifier
	%0A	HID	Hardware Interrupt Identifier
	%F0~%FF	X0~XF	Index Registers


Instruction Formats:
Zero Operand Instruction Format (Z):	(1 Byte)
+---+---+---+---+---+---+---+---+
|             O   P             |
+---+---+---+---+---+---+---+---+

One Operand (U):	(2 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           A   R   G           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
(ARG can be a register number or immediate operand depending on the instruction)

Two Operand (D):	(3 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           D   S   T           |           S   R   C           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

One and Quarter (IQ):	(4 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           R   E   G           |                       V   A   L   U   E                       |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
(VALUE:	16 Bit)

One and Full Word (IW):	(10 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           R   E   G           |	==>
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                                       V   A   L   U   E                                                       | (Lower Half)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                                       V   A   L   U   E                                                       | (Higher Half)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
(VALUE:	64 Bit)

One and Full Word (IW):	(10 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           R   E   G           |	==>
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                                       V   A   L   U   E                                                       |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
(VALUE:	32 Bit)

Jump/Call (JC):		(5 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |                                                       A   D   D   R                                                           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

Conditional Jump/Call (CJC):	(7 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           R   E   G           |           R   E   G           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                                   O   F   F   S   E   T                                                       | (Two's Completement OFFSET)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

LOAD / STORE Instruction Format (LS):	(6 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |   I N D E X   |IND| I |S I Z E|           R   E   G           |	==>
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                                       A   D   D   R                                                           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
(Size:	W = 64 Bit, H = 32 Bit, Q = 16 Bit, B = 8 Bit)
(I:	Indirect)
(IND:	Index)
(INDEX:	Which register to use (X0 ~ XF))

System Call (SYS):	(4 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           N   U   M           |           R   E   G           |           R   E   G           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

IO (IO):		(4 Bytes)
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|             O   P             |           D   E   V           |             O   P             |           R   E   G           |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
