IO Instructions
===============

Device:	CPU	(0x00~0x0F)
	BOOT:	Boot this CPU (Doesn't work on CPU0)
	HALT:	Halt this CPU (Doesn't work on CPU0)
	EI:	Enable Interrupt
	DI:	Disable
	ECLK:	Enable Clock Interrupt
	DCLK:	Disable
	PSR:	Read panel switches (to Register)
	DPY:	Display Custom data on panel display
	BTPC:	Set the bootstrap entry point
	BTSP:	Set the bootstrap stack pointer
	INFO:	Get CPU Information

CPU Information Format:
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                                                                                                               | Lower Half
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|																| Higher Half
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+


Device:	CONSOLE	(0x10)
	OUT:	Write one byte
	IN:	Read one byte
	CLR:	Clear screen
	BEL:	Send Bell
	RST:	Reset console
