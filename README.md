# NeoTECH General Purpose Computing System (GPCS)
## Introduction:

![NeoTECH GPCS Logo](https://gitlab.com/Neo_Chen/NeoTECH_GeneralPurposeComputingSystem/raw/master/LOGO/GPCS-256px.png "Yes, this is our logo")

NeoTECH G.P.C.S. is a 64 bit virtual machine with 64 Bit memory address.


Our goal is to create a modern "minicomputer" with Operating System, just like DEC's PDP-8 or PDP-11.


Because my programming level is somewhat poor, it may not reach the goal :(

	/* ========================================================================== *\
	||                                NeoTECH GPCS                                ||
	||              NeoTechnology - General Purpose Computing System              ||
	\* ========================================================================== */
